from django.urls import include, path
from rest_framework import routers
from compiler.views import CompileWorkflowView


router = routers.DefaultRouter()
router.register("compile", CompileWorkflowView)

urlpatterns = [
    path("api/", include(router.urls)),
]
