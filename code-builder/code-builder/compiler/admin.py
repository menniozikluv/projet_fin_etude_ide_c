from django.contrib import admin
from compiler.models import CompileWorkflow


class CompileWorkflowAdmin(admin.ModelAdmin):
    list_display = ["user", "created", "status"]
    list_filter = ["user", "created", "status"]
    search_fields = ["user", "created", "status"]


admin.site.register(CompileWorkflow, CompileWorkflowAdmin)
